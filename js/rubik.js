(function($){
	$.fn.rubik = function(obj){		
		//		Преобразование матрицы в строку
		var m2str = function(m){
			return m.inspect().replace(/\]\s\[/g, ',').replace(/[\[\] ]/g, '');
		};
		return this.each(function(){
			var rotate = function(cubes, line, ckw){
				if(cubes && cubes.size() == 9){
					isActive = 0;
					var i = 1;
					var int = setInterval(function(){
						if(i <= o.steps){
							cubes.each(function(){
								var cube = $(this);
								var a = Math.PI / 2 / o.steps * i * ckw;
								var m = cube.data('m');
								switch(line){
									case 0:
										if(i == o.steps){
											var mm = m.x($M([
												[1,0,0,0],
												[0,0,-ckw,0],
												[0,ckw,0,0],
												[0,0,0,1]
											]));
											cube.data('m', mm);
										} else {
											var mm = m.x($M([
												[1,0,0,0],
												[0,Math.cos(a),Math.sin(-a),0],
												[0,Math.sin(a),Math.cos(a),0],
												[0,0,0,1]
											]));
										}
										cube.css({
											transform: 'matrix3d(' + m2str(mm) + ')'
										});
										break;
									case 1:
										if(i == o.steps){
											var mm = m.x($M([
												[0,0,-ckw,0],
												[0,1,0,0],
												[ckw,0,0,0],
												[0,0,0,1]
											]));
											cube.data('m', mm);
										} else {
											var mm = m.x($M([
												[Math.cos(a),0,Math.sin(-a),0],
												[0,1,0,0],
												[Math.sin(a),0,Math.cos(a),0],
												[0,0,0,1]
											]));
										}
										cube.css({
											transform: 'matrix3d(' + m2str(mm) + ')'
										});
										break;
									case 2:
										if(i == o.steps){
											var mm = m.x($M([
												[0,-ckw,0,0],
												[ckw,0,0,0],
												[0,0,1,0],
												[0,0,0,1]
											]));
											cube.data('m', mm);
										} else {
											var mm = m.x($M([
												[Math.cos(a),Math.sin(-a),0,0],
												[Math.sin(a),Math.cos(a),0,0],
												[0,0,1,0],
												[0,0,0,1]
											]));
										}
										cube.css({
											transform: 'matrix3d(' + m2str(mm) + ')'
										});
										break;
								}
							});
							i++;
						} else {
							clearInterval(int);
						}
					}, o.tr / o.steps);
				}
			};
			var o = $.extend({
				a: 3,		//Угол поворота при нажатии на клавишу управления
				tr: 500,		//Время поворота грани
				steps: 12	//Разбиение поворота грани
			}, obj);
			var rubik = $(this);
			var parent = rubik.parent();
			rubik.m = $M([
				[1,0,0,0],
				[0,1,0,0],
				[0,0,1,0],
				[0,0,0,1]
			]);
			rubik.css({
				transform: 'matrix3d(1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1)'
			});
			var cubes = $('.cube', rubik);
			var s = $('.face', rubik).eq(0).outerWidth();
			cubes.each(function(i){
				var cube = $(this);
				x = i % 3 - 1;
				y = Math.floor(i % 9 / 3) - 1;
				z = Math.floor(i / 9) - 1;
				
				cube.data('m', $M([
					[1,0,0,0],
					[0,1,0,0],
					[0,0,1,0],
					[x*s,y*s,z*s,1]
				]));
				cube.css({
					top: s + 'px',
					left: s + 'px',
//					'transform-origin': s * 3 / 2 + 'px ' + s * 3 / 2 + 'px 0px',
					'transform-origin': 0.5 * s +'px ' + 0.5 * s + 'px 0px',
					transform: 'matrix3d(' + m2str(cube.data('m')) + ')'
				});

				var faces = $('.face', cube);
				faces.eq(0).css({
					transform: 'matrix3d(1,0,0,0,0,-1,0,0,0,0,-1,0,0,0,-' + s / 2 + ',1)'
				}).data('m', $M([
					[1,0,0,0],
					[0,-1,0,0],
					[0,0,-1,0],
					[0,0,-s/2,1],
				]));
				faces.eq(1).css({
					transform: 'matrix3d(1,0,0,0,0,1,0,0,0,0,1,0,0,0,' + s / 2 + ',1)'
				}).data('m', $M([
					[1,0,0,0],
					[0,1,0,0],
					[0,0,1,0],
					[0,0,s/2,1]
				]));
				faces.eq(2).css({
					transform: 'matrix3d(0,0,-1,0,0,1,0,0,1,0,0,0,' + s / 2 + ',0,0,1)'
				}).data('m', $M([
					[0,0,-1,0],
					[0,1,0,0],
					[1,0,0,0],
					[s/2,0,0,1]
				]));
				faces.eq(3).css({
					transform: 'matrix3d(0,0,1,0,0,1,0,0,-1,0,0,0,-' + s / 2 + ',0,0,1)'
				}).data('m', $M([
					[0,0,1,0],
					[0,1,0,0],
					[-1,0,0,0],
					[-s/2,0,0,1]
				]));
				faces.eq(4).css({
					transform: 'matrix3d(1,0,0,0,0,0,-1,0,0,1,0,0,0,' + s / 2 + ',0,1)'
				}).data('m', $M([
					[1,0,0,0],
					[0,0,-1,0],
					[0,1,0,0],
					[0,s/2,0,1]
				]));
				faces.eq(5).css({
					transform: 'matrix3d(1,0,0,0,0,0,1,0,0,-1,0,0,0,-' + s / 2 + ',0,1)'
				}).data('m', $M([
					[1,0,0,0],
					[0,0,1,0],
					[0,-1,0,0],
					[0,-s/2,0,1]
				]));
			});
			cubes.slice(18,27).children('.face:nth-child(2)').addClass('c_w').data('line',2).data('dir',1);
			cubes.slice(0,9).children('.face:nth-child(1)').addClass('c_y').data('line',2).data('dir',-1);
			cubes.slice(0,3).add(cubes.slice(9,12)).add(cubes.slice(18,21)).children('.face:nth-child(6)').addClass('c_b').data('line',1).data('dir',1);
			cubes.slice(6,9).add(cubes.slice(15,18)).add(cubes.slice(24,27)).children('.face:nth-child(5)').addClass('c_g').data('line',1).data('dir',-1);
			cubes.filter(':nth-child(3n)').children('.face:nth-child(3)').addClass('c_r').data('line',0).data('dir',1);
			cubes.filter(':nth-child(3n+1)').children('.face:nth-child(4)').addClass('c_o').data('line',0).data('dir',-1);
			//---------Вращение всего куба
			$(document).on('keydown',function(e){
				switch(e.keyCode){
					case 87:
						var d = -Math.PI / 180 * o.a;
						var mr = $M([
							[1, 0, 0, 0],
							[0, Math.cos(d), Math.sin(-d), 0],
							[0, Math.sin(d), Math.cos(d), 0],
							[0, 0, 0, 1]
						]);
						rubik.m = rubik.m.x(mr);
						rubik.css({
							transform: 'matrix3d(' + m2str(rubik.m) + ')'
						});
						break;
					case 83:
						var d = Math.PI / 180 * o.a;
						var mr = $M([
							[1, 0, 0, 0],
							[0, Math.cos(d), Math.sin(-d), 0],
							[0, Math.sin(d), Math.cos(d), 0],
							[0, 0, 0, 1]
						]);
						rubik.m = rubik.m.x(mr);
						rubik.css({
							transform: 'matrix3d(' + m2str(rubik.m) + ')'
						});
						break;
					case 65:
						var d = Math.PI / 180 * o.a;
						var mr = $M([
							[Math.cos(d), 0, Math.sin(d), 0],
							[0, 1, 0, 0],
							[Math.sin(-d), 0, Math.cos(d), 0],
							[0, 0, 0, 1]
						]);
						rubik.m = rubik.m.x(mr);
						rubik.css({
							transform: 'matrix3d(' + m2str(rubik.m) + ')'
						});
						break;
					case 68:
						var d = -Math.PI / 180 * o.a;
						var mr = $M([
							[Math.cos(d), 0, Math.sin(d), 0],
							[0, 1, 0, 0],
							[Math.sin(-d), 0, Math.cos(d), 0],
							[0, 0, 0, 1]
						]);
						rubik.m = rubik.m.x(mr);
						rubik.css({
							transform: 'matrix3d(' + m2str(rubik.m) + ')'
						});
						break;
					case 81:
						var d = -Math.PI / 180 * o.a;
						var mr = $M([
							[Math.cos(d), Math.sin(d), 0, 0],
							[Math.sin(-d), Math.cos(d), 0, 0],
							[0, 0, 1, 0],
							[0, 0, 0, 1]
						]);
						rubik.m = rubik.m.x(mr);
						rubik.css({
							transform: 'matrix3d(' + m2str(rubik.m) + ')'
						});
						break;
					case 69:
						var d = Math.PI / 180 * o.a;
						var mr = $M([
							[Math.cos(d), Math.sin(d), 0, 0],
							[Math.sin(-d), Math.cos(d), 0, 0],
							[0, 0, 1, 0],
							[0, 0, 0, 1]
						]);
						rubik.m = rubik.m.x(mr);
						rubik.css({
							transform: 'matrix3d(' + m2str(rubik.m) + ')'
						});
						break;
					default:
				}
			});
			//---------Вращение граней
			var isActive = 0;
			rubik.on('mousedown', function(){
				isActive = 1;
				return false;		//Отмена перетаскивания
			});
			$(document).on('mouseup mouseleave', function(){
				isActive = 0;
			});
			$('.cube', rubik).on('mouseleave', function(e){
				if(isActive && $(e.toElement).is('.face')){
					var face = $(e.toElement);
					var mc1 = $(this).data('m');
					var mc2 = $(e.toElement).parent('.cube').data('m');
					if(mc1.e(4,1) == mc2.e(4,1) && face.data('line') != 0){
						var items = cubes.filter(function(){
							return $(this).data('m').e(4,1) == mc1.e(4,1);
						});
						var line = 0;
						if(face.data('line') == 2){
							var ckw = face.data('dir') * (mc2.e(4,2) > mc1.e(4,2) ? 1 : -1);
						} else {
							var ckw = face.data('dir') * (mc2.e(4,3) > mc1.e(4,3) ? 1 : -1);
						}
						items.children('.face').each(function(){
							if($(this).data('line') == 2){
								$(this).data('line', 1);
								if($(this).data('dir') == 1){
									$(this).data('dir', -ckw);
								} else if($(this).data('dir') == -1){
									$(this).data('dir', ckw);
								}
							}else if($(this).data('line') == 1){
								$(this).data('line', 2);
								if($(this).data('dir') == 1){
									$(this).data('dir', ckw);
								} else if($(this).data('dir') == -1){
									$(this).data('dir', -ckw);
								}
							}
						});
					} else if(mc1.e(4,2) == mc2.e(4,2) && face.data('line') != 1){
						var items = cubes.filter(function(){
							return $(this).data('m').e(4,2) == mc1.e(4,2);
						});
						var line = 1;
						if(face.data('line') == 2){
							var ckw = face.data('dir') * (mc2.e(4,1) > mc1.e(4,1) ? 1 : -1);
						} else {
							var ckw = face.data('dir') * (mc2.e(4,3) > mc1.e(4,3) ? -1 : 1);
						}
						items.children('.face').each(function(){
							if($(this).data('line') == 0){
								$(this).data('line', 2);
								if($(this).data('dir') == 1){
									$(this).data('dir', -ckw);
								} else if($(this).data('dir') == -1){
									$(this).data('dir', ckw);
								}
							}else if($(this).data('line') == 2){
								$(this).data('line', 0);
								if($(this).data('dir') == 1){
									$(this).data('dir', ckw);
								} else if($(this).data('dir') == -1){
									$(this).data('dir', -ckw);
								}
							}
						});
					} else if(mc1.e(4,3) == mc2.e(4,3) && face.data('line') != 2){
						var items = cubes.filter(function(){
							return $(this).data('m').e(4,3) == mc1.e(4,3);
						});
						var line = 2;
						if(face.data('line') == 1){
							var ckw = face.data('dir') * (mc2.e(4,1) > mc1.e(4,1) ? -1 : 1);
						} else {
							var ckw = face.data('dir') * (mc2.e(4,2) > mc1.e(4,2) ? -1 : 1);
						}
						items.children('.face').each(function(){
							if($(this).data('line') == 0){
								$(this).data('line', 1);
								if($(this).data('dir') == 1){
									$(this).data('dir', ckw);
								} else if($(this).data('dir') == -1){
									$(this).data('dir', -ckw);
								}
							}else if($(this).data('line') == 1){
								$(this).data('line', 0);
								if($(this).data('dir') == 1){
									$(this).data('dir', -ckw);
								} else if($(this).data('dir') == -1){
									$(this).data('dir', ckw);
								}
							}
						});
					}
					rotate(items, line, ckw);
				}
			});
		});
	};
})(jQuery);